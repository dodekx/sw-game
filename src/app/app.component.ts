import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { comparePeople, init } from './store/appState/actions';
import { Observable } from 'rxjs';
import { selectPeopleCount } from './store/people/selectors';
import { displayCategoryCards, displayPersonalCards, } from './store/appState/selectors';
import { selectShipCount } from './store/ships/selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  numberOfPeople$: Observable<number>;
  numberOfShip$: Observable<number>;
  displayCategoryCards$: Observable<boolean>;
  displayPersonalCards$: Observable<boolean>;

  constructor(private readonly store: Store) {
  }

  ngOnInit(): void {
    this.store.dispatch(init());
    this.numberOfPeople$ = this.store.select(selectPeopleCount);
    this.numberOfShip$ = this.store.select(selectShipCount);
    this.displayCategoryCards$ = this.store.select(displayCategoryCards);
    this.displayPersonalCards$ = this.store.select(displayPersonalCards);
  }

  selectComparePeople() {
    this.store.dispatch(comparePeople());
  }

}
