import { async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Store } from '@ngrx/store';
import { deepEqual, instance, mock, resetCalls, verify, when } from 'ts-mockito';
import { init } from './store/appState/actions';
import { selectPeopleCount } from './store/people/selectors';
import { of } from 'rxjs';
import { selectShipCount } from './store/ships/selectors';

describe('AppComponent unit test ', () => {
  let component: AppComponent;
  const store$: Store = mock(Store);

  beforeEach(async(() => {
    resetCalls(store$);
    component = new AppComponent(instance(store$));

    when(store$.select(selectPeopleCount)).thenCall(() => of(10));
    when(store$.select(selectShipCount)).thenCall(() => of(10));
  }));

  it('on init should call init action', () => {
    component.ngOnInit();
    verify(store$.dispatch(deepEqual(init()))).once();
  });

});
