export enum LoadState {
  loading = 'loading',
  loaded = 'loaded',
  error = 'error',
  none = 'none',
}
