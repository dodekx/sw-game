import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeopleOponentCardComponent } from './people-oponent-card.component';
import { Store } from '@ngrx/store';
import { instance, mock } from 'ts-mockito';

describe('PeopleOponentCardComponent', () => {
  let component: PeopleOponentCardComponent;
  let fixture: ComponentFixture<PeopleOponentCardComponent>;
  const store = mock(Store);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PeopleOponentCardComponent],
      providers: [
        {provide: Store, useValue: instance(store)}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleOponentCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
