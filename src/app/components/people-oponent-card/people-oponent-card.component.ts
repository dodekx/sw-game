import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { People } from '../../services/people.service';
import { Store } from '@ngrx/store';
import { selectUsersCard } from '../../store/people/selectors';

@Component({
  selector: 'app-people-oponent-card',
  templateUrl: './people-oponent-card.component.html',
  styleUrls: ['./people-oponent-card.component.scss']
})
export class PeopleOponentCardComponent implements OnInit {

  person$: Observable<People>;

  constructor(private readonly store$: Store) {
  }

  ngOnInit(): void {
    this.person$ = this.store$.select(selectUsersCard);
  }


  @Input()
  hideDetails = true;

  hideIfNecesary(details) {
    return this.hideDetails ? '???' : details;
  }
}
