import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeopleUserCardComponent } from './people-user-card.component';
import { Store } from '@ngrx/store';
import { instance, mock } from 'ts-mockito';

describe('PeopleUserCardComponent', () => {
  let component: PeopleUserCardComponent;
  let fixture: ComponentFixture<PeopleUserCardComponent>;
  const store = mock(Store);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PeopleUserCardComponent],
      providers: [
        {provide: Store, useValue: instance(store)}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleUserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
