import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { People } from '../../services/people.service';
import { selectOpponentCard } from '../../store/people/selectors';
import { comparePersonCard } from '../../store/people/actions';

@Component({
  selector: 'app-people-user-card',
  templateUrl: './people-user-card.component.html',
  styleUrls: ['./people-user-card.component.scss']
})
export class PeopleUserCardComponent implements OnInit {

  person$: Observable<People>;

  constructor(private readonly store$: Store) {
  }

  ngOnInit(): void {
    this.person$ = this.store$.select(selectOpponentCard);
  }

  select(key: keyof People) {
    this.store$.dispatch(comparePersonCard({key}));
  }


}
