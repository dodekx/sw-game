import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { selectGameResult } from '../../store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-game-result',
  templateUrl: './game-result.component.html',
  styleUrls: ['./game-result.component.scss']
})
export class GameResultComponent implements OnInit {

  gameResult$: Observable<{
    player: number;
    opponent: number;
  }>;

  constructor(private readonly store: Store) {
  }


  ngOnInit(): void {
    this.gameResult$ = this.store.select(selectGameResult);

  }

}
