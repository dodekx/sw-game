import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameResultComponent } from './game-result.component';
import { Store } from '@ngrx/store';
import { instance, mock } from 'ts-mockito';

describe('GameResultComponent', () => {
  let component: GameResultComponent;
  let fixture: ComponentFixture<GameResultComponent>;
  const store = mock(Store);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameResultComponent],
      providers: [
        {provide: Store, useValue: instance(store)}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
