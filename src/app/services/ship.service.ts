import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ShipService {

  private readonly configUrl = 'https://swapi.dev/api/starships/';

  constructor(private readonly http: HttpClient) {
  }

  getQuantity() {
    return this.http.get<ShipConfig>(this.configUrl).pipe(
      map(res => res.count)
    );
  }

  getShip(id: number) {
    return this.http.get<ShipReq>(`${this.configUrl}${id}/`)
      .pipe(
        map(shipReqToShip)
      );
  }
}


interface ShipConfig {
  count: number;
}

interface ShipReq {
  'name': string;
  'model': string;
  'manufacturer': string;
  'cost_in_credits': string;
  'length': string;
  'max_atmosphering_speed': string;
  'crew': string;
  'passengers': string;
  'cargo_capacity': string;
  'consumables': string;
  'hyperdrive_rating': string;
  'MGLT': string;
  'starship_class': string;
}

interface Ship {
  'name': string;
  'model': string;
  'manufacturer': string;
  'cost_in_credits': number;
  'length': number;
  'max_atmosphering_speed': number | null;
  'crew': number;
  'passengers': number;
  'cargo_capacity': number;
  'consumables': string;
  'hyperdrive_rating': string;
  'MGLT': number;
  'starship_class': string;
}

export const shipReqToShip = (shipReq: ShipReq): Ship => {
  return {
    ...shipReq,
    cost_in_credits: parseInt(shipReq.cost_in_credits, 10),
    length: parseInt(shipReq.length, 10),
    max_atmosphering_speed: parseInt(shipReq.max_atmosphering_speed, 10),
    crew: parseInt(shipReq.crew, 10),
    passengers: parseInt(shipReq.passengers, 10),
    cargo_capacity: parseInt(shipReq.cargo_capacity, 10),
    MGLT: parseInt(shipReq.MGLT, 10),
  };
};
