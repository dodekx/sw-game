// Http testing module and mocking controller
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { People, PeopleService } from './people.service';
import { samplePeopleMock } from './peopleMock';

describe('PeopleService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: PeopleService;

  const URL = 'https://swapi.dev/api/people/';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    // Inject the http service and test controller for each test
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.get(PeopleService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be return number of people in api', () => {
    const count = 82;
    service.getQuantity().subscribe(res => {
      expect(res).toEqual(count);
    });

    httpTestingController
      .expectOne(req => req.method === 'GET' && req.url === URL)
      .flush({
          count,
          next: 'http://swapi.dev/api/people/?page=2',
          previous: null,
        }
        , {status: 200, statusText: ''});

    httpTestingController.verify();
  });

  it('should be return  people ', () => {
    service.getPerson(1).subscribe(res => {
      const {name, birth_year, eye_color, gender, hair_color, height, mass, skin_color} = samplePeopleMock;
      expect(res).toEqual({
        name, birth_year, eye_color, gender, hair_color, height: parseInt(height, 10), mass: parseInt(mass, 10), skin_color
      } as People);
    });
    const URL_REQ = URL + '1/';
    httpTestingController
      .expectOne(req => req.method === 'GET' && req.url === URL_REQ)
      .flush(
        samplePeopleMock
        , {status: 200, statusText: ''});

    httpTestingController.verify();
  });
});

