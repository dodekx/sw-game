// Http testing module and mocking controller
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { ShipService } from './ship.service';

describe('ShipService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: ShipService;

  const URL = 'https://swapi.dev/api/starships/';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    // Inject the http service and test controller for each test
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.get(ShipService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be return number of people in api', () => {
    const count = 82;
    service.getQuantity().subscribe(res => {
      expect(res).toEqual(count);
    });

    httpTestingController
      .expectOne(req => req.method === 'GET' && req.url === URL)
      .flush({
          count,
          next: 'http://swapi.dev/api/people/?page=2',
          previous: null,
        }
        , {status: 200, statusText: ''});

    httpTestingController.verify();
  });

});
