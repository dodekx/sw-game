import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  private readonly configUrl = 'https://swapi.dev/api/people/';

  constructor(private readonly http: HttpClient) {
  }

  getQuantity() {
    return this.http.get<PeopleConfig>(this.configUrl).pipe(
      map(res => res.count)
    );
  }

  getPerson(id: number) {
    return this.http.get<PeopleReq>(`${this.configUrl}${id}/`).pipe(
      map(peopleReqToPeople)
    );
  }
}


interface PeopleConfig {
  count: number;
}

export interface People {
  name: string;
  height: number | null;
  mass: number | null;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;

}

export interface PeopleReq {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;

}

const toNumberOrNull = (conversionString: string): null | number => {
  const conversion = parseInt(conversionString, 10);
  return Number.isNaN(conversion) ? null : conversion;
};

export const peopleReqToPeople = ({name, birth_year, eye_color, gender, hair_color, height, mass, skin_color}: PeopleReq): People => ({
  name,
  hair_color,
  skin_color,
  eye_color,
  birth_year,
  gender,
  height: toNumberOrNull(height),
  mass: toNumberOrNull(mass),
});

