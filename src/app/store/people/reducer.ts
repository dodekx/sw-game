import { Action, createReducer, on } from '@ngrx/store';
import {
  loadPeopleCounter,
  loadPeopleCounterFailure,
  loadPeopleCounterSuccess,
  loadPeopleOpponentCardSuccess,
  loadPeopleUserCardSuccess
} from './actions';
import { LoadState } from '../../models/loadState';
import { People } from '../../services/people.service';

export interface State {
  count: number;
  loadState: LoadState;
  usersCard: People | undefined;
  opponentCard: People | undefined;

}

export const initialState: State = {
  count: 0,
  loadState: LoadState.none,
  usersCard: undefined,
  opponentCard: undefined,
};

export const peopleReducer = createReducer(
  initialState,
  on(loadPeopleCounterSuccess, (state, {count}) => {
    return {
      ...state,
      count,
      loadState: LoadState.loaded
    };
  }),

  on(loadPeopleCounter, (state) => ({...state, loadState: LoadState.loading})),
  on(loadPeopleCounterFailure, (state) => ({...state, loadState: LoadState.error})),
  on(loadPeopleUserCardSuccess, (state, action) => ({...state, usersCard: action.people})),
  on(loadPeopleOpponentCardSuccess, (state, action) => ({...state, opponentCard: action.people})),
);

export function reducer(state: State | undefined, action: Action) {
  return peopleReducer(state, action);
}
