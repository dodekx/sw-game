import { createAction, props } from '@ngrx/store';
import { People } from '../../services/people.service';


export const loadPeopleCounter = createAction(
  '[People] Load ',
);

export const loadPeopleCounterSuccess = createAction(
  '[People] Load Success',
  props<{ count: number }>()
);

export const loadPeopleCounterFailure = createAction(
  '[People] Load fail',
  props<{ error: string }>()
);

export const loadPeopleUserCard = createAction(
  '[People] load People User Card ',
);

export const loadPeopleUserCardSuccess = createAction(
  '[People] load People User Card Success',
  props<{ people: People }>()
);

export const loadPeopleUserCardFailure = createAction(
  '[People] load People User Card fail',
  props<{ error: string }>()
);

export const loadPeopleOpponentCard = createAction(
  '[People] load Opponent User Card ',
);

export const loadPeopleOpponentCardSuccess = createAction(
  '[People] load People Opponent Card Success',
  props<{ people: People }>()
);

export const loadPeopleOpponentCardFailure = createAction(
  '[People] load People Opponent Card fail',
  props<{ error: string }>()
);

export const comparePersonCard = createAction(
  '[People] compare Person Cards cards',
  props<{ key: keyof People }>()
);
