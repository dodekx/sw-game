import { cold, hot } from 'jasmine-marbles';
import { loadPeopleCounter, loadPeopleCounterSuccess, loadPeopleOpponentCardSuccess } from './actions';
import { Effects } from './effects';
import { peopleReqToPeople, PeopleService } from '../../services/people.service';
import { anything, instance, mock, resetCalls, verify, when } from 'ts-mockito';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { samplePeopleMock } from '../../services/peopleMock';
import { selectPeopleCount } from './selectors';
import { MatSnackBar } from '@angular/material/snack-bar';


describe('Effects', () => {
  const peopleService = mock(PeopleService);
  const store$ = mock(Store);
  const matSnackBar = mock(MatSnackBar);

  describe('loadPeopleCounter$ ', () => {
    it('when loadPeopleCounter$ successful  then  should be dispatched loadPeopleCounterSuccess', () => {
      const action$ = hot('a', {a: loadPeopleCounter()});

      const effects = new Effects(action$, instance(peopleService), instance(store$), instance(matSnackBar));

      when(peopleService.getQuantity()).thenCall(() => of(53));

      const expectedResult$ = cold('c', {
        c: loadPeopleCounterSuccess({count: 53})
      });

      expect(effects.loadPeopleCounter$).toBeObservable(expectedResult$);

    });
  });

// TODO veryfied what is wrong here
  xdescribe('loadPeopleOpponentCard$ ', () => {
    beforeEach(() => {
      resetCalls(store$);
      resetCalls(peopleService);
      when(store$.select(selectPeopleCount)).thenCall(() => of(4));
      when(peopleService.getPerson(anything())).thenReturn(
        cold('-a', {a: (peopleReqToPeople(samplePeopleMock))})
      );

    });

    it('when loadPeopleOpponentCard$ successful  then  should be dispatched loadPeopleOpponentCardSuccess', () => {
      const action$ = hot('a', {a: loadPeopleCounter()});

      const effects = new Effects(action$, instance(peopleService), instance(store$), instance(matSnackBar));

      const expectedResult$ = cold('-c', {
        c: loadPeopleOpponentCardSuccess({people: peopleReqToPeople(samplePeopleMock)})
      });

      verify(store$.select(selectPeopleCount)).called();
      verify(peopleService.getPerson(anything())).once();
      expect(effects.loadPeopleOpponentCard$).toBeObservable(expectedResult$);

    });
  });

});
