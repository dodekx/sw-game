import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { PeopleService } from '../../services/people.service';
import {
  comparePersonCard,
  loadPeopleCounter,
  loadPeopleCounterFailure,
  loadPeopleCounterSuccess,
  loadPeopleOpponentCard,
  loadPeopleOpponentCardFailure,
  loadPeopleOpponentCardSuccess,
  loadPeopleUserCard,
  loadPeopleUserCardSuccess
} from './actions';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { selectOpponentCard, selectPeopleCount, selectUsersCard } from './selectors';
import { oponentWin, playerWin } from '../game-result';
import { init } from '../appState';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class Effects {

  loadPeopleCounter$ = createEffect(() => this.actions$.pipe(
    ofType(loadPeopleCounter.type),
    mergeMap(() => this.peopleService.getQuantity()
      .pipe(
        map(count => (loadPeopleCounterSuccess({count}))),
        catchError(() => of(loadPeopleCounterFailure({error: 'not able count peole'}))),
      ))
    )
  );

  loadPeopleUserCard$ = createEffect(() => this.actions$.pipe(
    ofType(loadPeopleUserCard.type),
    withLatestFrom(
      this.store$.select(selectPeopleCount),
      (action, counter) => (counter)
    ),
    switchMap((counter) => {
        const randomNumber = Math.floor(Math.random() * counter + 1);
        return this.peopleService.getPerson(randomNumber)
          .pipe(
            map(res => (loadPeopleUserCardSuccess({people: res}))),
            catchError(() => of(loadPeopleCounterFailure({error: 'not able load people'}))),
          );
      }
    )
  ));

  loadPeopleOpponentCard$ = createEffect(() => this.actions$.pipe(
    ofType(loadPeopleOpponentCard.type),
    withLatestFrom(
      this.store$.select(selectPeopleCount),
      (action, counter) => (counter)
    ),
    mergeMap((counter) => {
        const randomNumber = Math.floor(Math.random() * counter + 1);
        return this.peopleService.getPerson(randomNumber)
          .pipe(
            map(res => (loadPeopleOpponentCardSuccess({people: res}))),
            catchError(() => of(loadPeopleOpponentCardFailure({error: 'not able load people'}))),
          );
      }
    )
  ));


  comparePersonCard$ = createEffect(() => this.actions$.pipe(
    ofType(comparePersonCard.type),
    withLatestFrom(
      this.store$.select(selectUsersCard),
      this.store$.select(selectOpponentCard),
      (action: ReturnType<typeof comparePersonCard>, user, oponent) => {

        return !!user[action.key] && (user[action.key] > oponent[action.key]);
      }
    ),
    mergeMap((isWin) => {
      //  todo add test
      this.snackBar.open(`You ${isWin ? 'win' : 'lose'}`, 'next');
      return [
        isWin ? playerWin() : oponentWin(),
        init()
      ];
    })
  ));

  constructor(
    private actions$: Actions,
    private peopleService: PeopleService,
    private store$: Store,
    private snackBar: MatSnackBar
  ) {
  }
}

