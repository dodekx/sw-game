import { createSelector } from '@ngrx/store';
import { State } from '..';


export const selectPeople = (state: State) => state.people;

export const selectPeopleCount = createSelector(
  selectPeople,
  (state: ReturnType<typeof selectPeople>) => state.count
);

export const selectUsersCard = createSelector(
  selectPeople,
  (state: ReturnType<typeof selectPeople>) => state.usersCard
);

export const selectOpponentCard = createSelector(
  selectPeople,
  (state: ReturnType<typeof selectPeople>) => state.opponentCard
);
