import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import * as fromPeople from './people';
import * as fromAppState from './appState';
import * as fromShip from './ships';
import * as fromGameResult from './game-result';

export interface State {
  people: fromPeople.State;
  ship: fromShip.Ship;
  appState: fromAppState.State;
  gameResult: fromGameResult.State;

}

export const reducers: ActionReducerMap<State> = {
  people: fromPeople.reducer,
  appState: fromAppState.reducer,
  ship: fromShip.reducer,
  gameResult: fromGameResult.reducer,
};


export const metaReducers: MetaReducer<State>[] = [];
export const effects = [
  fromPeople.Effects,
  fromAppState.Effects,
  fromShip.Effects,

];

export const selectGameResult = (state: State) => state.gameResult;
