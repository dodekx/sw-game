import { createSelector } from '@ngrx/store';
import { State } from '..';


export const selectShip = (state: State) => state.ship;

export const selectShipCount = createSelector(
  selectShip,
  (state: ReturnType<typeof selectShip>) => state.count
);
