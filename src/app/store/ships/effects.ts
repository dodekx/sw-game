import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { loadShipCounter, loadShipCounterFailure, loadShipCounterSuccess } from './actions';
import { ShipService } from '../../services/ship.service';

@Injectable()
export class Effects {

  loadPeopleCounter = createEffect(() => this.actions$.pipe(
    ofType(loadShipCounter.type),
    mergeMap(() => this.shipService.getQuantity()
      .pipe(
        map(count => (loadShipCounterSuccess({count}))),
        catchError(() => loadShipCounterFailure)
      ))
    )
  );

  constructor(
    private actions$: Actions,
    private shipService: ShipService
  ) {
  }
}
