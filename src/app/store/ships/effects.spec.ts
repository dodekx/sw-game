import { cold, hot } from 'jasmine-marbles';
import { Effects } from './effects';
import { instance, mock, when } from 'ts-mockito';
import { of } from 'rxjs';
import { ShipService } from '../../services/ship.service';
import { loadShipCounter, loadShipCounterSuccess } from './actions';


describe('Effects', () => {
  const shipService = mock(ShipService);

  describe('loadShipCounter ', () => {
    it('when loadPeopleCounter$ successful update then  should be dispatched loadPeopleCounterSuccess', () => {
      const action$ = hot('a', {a: loadShipCounter()});

      const effects = new Effects(action$, instance(shipService));

      when(shipService.getQuantity()).thenCall(() => of(53));

      const expectedResult$ = cold('c', {
        c: loadShipCounterSuccess({count: 53})
      });

      expect(effects.loadPeopleCounter).toBeObservable(expectedResult$);

    });
  });

});
