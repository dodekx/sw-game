import * as fromReducer from './reducer';
import { loadShipCounter, loadShipCounterFailure, loadShipCounterSuccess } from './actions';
import { LoadState } from '../../models/loadState';

describe('Reducer', () => {


  it('loadPeopleCounterSuccess should set number of people as 4', () => {

    const res = fromReducer.peopleReducer(fromReducer.initialState, loadShipCounterSuccess({count: 4}));
    expect(res.count).toEqual(4);
    expect(res.loadState).toEqual(LoadState.loaded);

  });

  it('loadPeopleCounter should set people state as loading', () => {

    const res = fromReducer.peopleReducer(fromReducer.initialState, loadShipCounter());
    expect(res.loadState).toEqual(LoadState.loading);

  });

  it('loadPeopleCounterFails should set people state as error', () => {

    const res = fromReducer.peopleReducer(fromReducer.initialState, loadShipCounterFailure({error: 'error'}));
    expect(res.loadState).toEqual(LoadState.error);

  });

});
