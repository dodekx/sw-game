import { createAction, props } from '@ngrx/store';


export const loadShipCounter = createAction(
  '[Ship] Load ',
);

export const loadShipCounterSuccess = createAction(
  '[Ship] Load Success',
  props<{ count: number }>()
);

export const loadShipCounterFailure = createAction(
  '[Ship] Load fail',
  props<{ error: any }>()
);

