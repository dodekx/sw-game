import { Action, createReducer, on } from '@ngrx/store';
import { loadShipCounter, loadShipCounterFailure, loadShipCounterSuccess } from './actions';
import { LoadState } from '../../models/loadState';

export interface Ship {
  count: number;
  loadState: LoadState;
}

export const initialState: Ship = {
  count: 0,
  loadState: LoadState.none,
};

export const peopleReducer = createReducer(
  initialState,
  on(loadShipCounterSuccess, (state, {count}) => {
    return {
      ...state,
      count,
      loadState: LoadState.loaded
    };
  }),

  on(loadShipCounter, (state) => ({...state, loadState: LoadState.loading})),
  on(loadShipCounterFailure, (state) => ({...state, loadState: LoadState.error})),
);

export function reducer(state: Ship | undefined, action: Action) {
  return peopleReducer(state, action);
}
