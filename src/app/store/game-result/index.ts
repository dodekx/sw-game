import { Action, createAction, createReducer, on } from '@ngrx/store';

export const playerWin = createAction(
  '[Player] player Win ',
);

export const oponentWin = createAction(
  '[Player] oponent Win ',
);


export interface State {
  player: number;
  opponent: number;
}

export const initialState: State = {
  player: 0,
  opponent: 0,
};

export const peopleReducer = createReducer(
  initialState,
  on(
    playerWin, (state) => ({
      ...state,
      player: state.player + 1
    })
  ),
  on(
    oponentWin, (state) => ({
      ...state,
      opponent: state.opponent + 1
    })
  ),
);

export function reducer(state: State | undefined, action: Action) {
  return peopleReducer(state, action);
}


