import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { mergeMap } from 'rxjs/operators';
import { comparePeople, init } from './actions';
import { loadPeopleCounter, loadPeopleOpponentCard, loadPeopleUserCard } from '../people/actions';
import { loadShipCounter } from '../ships/actions';

@Injectable()
export class Effects {

  init$ = createEffect(() => this.actions$.pipe(
    ofType(init.type),
    mergeMap(() => [
      loadPeopleCounter(),
      loadShipCounter(),
    ])
    )
  );

  comparePeople$ = createEffect(() => this.actions$.pipe(
    ofType(comparePeople.type),
    mergeMap(() => [
      loadPeopleUserCard(),
      loadPeopleOpponentCard(),
    ])
    )
  );

  // TODO init errors dialog

  constructor(
    private actions$: Actions,
  ) {
  }
}
