import { Action, createReducer, on } from '@ngrx/store';
import { comparePeople, init } from './actions';
import { oponentWin, playerWin } from '../game-result';

export enum AppStateEnum {
  init = 'init',
  comparePeople = 'comparePeople',
  compareShip = 'compareShip',
  summary = 'summary',
  error = 'error',
}

export interface State {
  state: AppStateEnum;

}

export const initialState: State = {
  state: AppStateEnum.init,

};

export const peopleReducer = createReducer(
  initialState,
  on(init, (state) => {
    return {
      ...state,
      state: AppStateEnum.init
    };
  }),
  on(comparePeople, (state) => {
    return {
      ...state,
      state: AppStateEnum.comparePeople
    };
  }),
  on(playerWin, (state) => {
    return {
      ...state,
      state: AppStateEnum.summary
    };
  }),
  on(oponentWin, (state) => {
    return {
      ...state,
      state: AppStateEnum.summary
    };
  }),
);

export function reducer(state: State | undefined, action: Action) {
  return peopleReducer(state, action);
}
