import { State } from '..';
import { createSelector } from '@ngrx/store';
import { AppStateEnum } from './reducer';


export const appState = (state: State) => state.appState.state;

export const displayCategoryCards = createSelector(
  appState,
  (state: ReturnType<typeof appState>): boolean => state === AppStateEnum.init
);
export const displayPersonalCards = createSelector(
  appState,
  (state: ReturnType<typeof appState>): boolean => state === AppStateEnum.comparePeople
);

