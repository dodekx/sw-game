import { createAction, props } from '@ngrx/store';

export const init = createAction(
  '[AppState] init'
);

export const initSuccess = createAction(
  '[AppState] init Success',
  props<{ count: number }>()
);

export const initFailure = createAction(
  '[AppState] init fail',
  props<{ error: any }>()
);

export const comparePeople = createAction(
  '[AppState] compare people'
);

