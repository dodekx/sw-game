import { cold, hot } from 'jasmine-marbles';
import { init } from './actions';
import { Effects } from './effects';
import { PeopleService } from '../../services/people.service';
import { mock, when } from 'ts-mockito';
import { of } from 'rxjs';
import { loadPeopleCounter } from '../people/actions';
import { loadShipCounter } from '../ships/actions';


describe('Effects', () => {
  const peopleService = mock(PeopleService);

  describe('init$ ', () => {
    it('init$ should init all necesary data ', () => {
      const action$ = hot('a', {a: init()});

      const effects = new Effects(action$);

      when(peopleService.getQuantity()).thenCall(() => of(53));

      const expectedResult$ = cold('(cd)', {
        c: loadPeopleCounter(),
        d: loadShipCounter()
      });

      expect(effects.init$).toBeObservable(expectedResult$);

    });
  });

});
